from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator


USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.

class MovieReview(models.Model):
    reviewer = models.ForeignKey(USER_MODEL, related_name="reviews", on_delete=models.CASCADE, null=True)
    reviewed_on = models.DateField(auto_now_add=True, null=True)
    title = models.CharField(max_length=60)
    poster = models.URLField(null=True, blank=True)
    release_year = models.IntegerField(
        default=2022,
        validators=[
            MaxValueValidator(2025),
            MinValueValidator(1900)
        ],
    )
    director = models.CharField(max_length=60)
    overview = models.TextField(max_length=400)
    review = models.TextField()
    rating = models.IntegerField(
        default=5,
        validators=[
            MaxValueValidator(10),
            MinValueValidator(1),
        ]
    )

    def __str__(self):
        return str(self.title) + " reviewed by " + str(self.reviewer)


class WatchlistItem(models.Model):
    owner = models.ForeignKey(USER_MODEL, related_name="watchlist_item", on_delete=models.CASCADE)
    movie = models.ForeignKey("MovieReview", related_name="watchlist_entry", on_delete=models.CASCADE)
    watch_status = models.BooleanField(verbose_name="Watched", default=False, blank=False)

    def __str__(self):
        return str(self.owner) + " wants to watch " + str(self.movie)
