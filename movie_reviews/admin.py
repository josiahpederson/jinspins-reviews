from django.contrib import admin
from movie_reviews.models import MovieReview, WatchlistItem
# Register your models here.

class MovieReviewAdmin(admin.ModelAdmin):
    pass


class WatchlistItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(MovieReview, MovieReviewAdmin)
admin.site.register(WatchlistItem, WatchlistItemAdmin)