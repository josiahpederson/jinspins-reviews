from django import forms

try:
    from movie_reviews.models import MovieReview

    class MovieForm(forms.ModelForm):
        class Meta:
            model = MovieReview
            fields = [
                "title",
                "poster",
                "release_year",
                "director",
                "rating",
                "overview",
                "review",
            ]

except Exception:
    pass