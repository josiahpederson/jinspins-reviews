from django.db import IntegrityError
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from movie_reviews.models import MovieReview, WatchlistItem
from django.contrib.auth.mixins import LoginRequiredMixin
from movie_reviews.forms import MovieForm
from django.shortcuts import redirect
from django.views.decorators.http import require_http_methods
from django.shortcuts import (get_object_or_404,
                              render,
                              HttpResponseRedirect)


# Create your views here.

class MoviesListView(ListView):
    model = MovieReview
    # context_object_name = "movies_list"
    template_name = "movie_reviews/list.html"
    paginate_by = 9

    def get_queryset(self):
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return MovieReview.objects.filter(title__icontains=query)


# class MovieReviewCreateView(LoginRequiredMixin, CreateView):
#     model = MovieReview
#     template_name = "movie_reviews/new.html"
#     fields = ["title", "poster", "release_year", "director", "overview", "review", "rating"]
#     success_url = reverse_lazy('movies_list')

#     # def get_context_data(self, **kwargs):
#     #     context = super().get_context_data(**kwargs)
#     #     user = self.request.user
#     #     context["reviewer"] = user

#     def form_valid(self, form):
#         form.instance.reviewer = self.request.user
#         return super().form_valid(form)


def create_movie_review(request):
    if request.method == "POST" and MovieForm and request.user.is_authenticated:
        form = MovieForm(request.POST)
        # form["reviewer"] = request.user
        if form.is_valid():
            form.instance.reviewer=request.user
            form.save()
            return redirect("movies_list")
    elif MovieForm:
        form = MovieForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "movie_reviews/new2.html", context)


def movie_review_detail(request, pk):
    reviewed_movie = MovieReview.objects.get(pk=pk)
    try:
        on_watchlist = request.user.watchlist_item.get(movie=reviewed_movie)
    except:
        on_watchlist = None
    context = {
        "movie": reviewed_movie if MovieReview else None,
        "watchlist_item_id": on_watchlist,
    }
    # movies_on_watchlist = []
    # for movie in request.user.watchlist_item.all():
    #     movies_on_watchlist.append(movie.movie)
    # context["movies_on_watchlist"] = movies_on_watchlist

    return render(request, "movie_reviews/details.html", context)


def update_review(request, pk):
    if MovieReview and MovieForm:
        instance = MovieReview.objects.get(pk=pk)
        if request.method == "POST":
            form = MovieForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect('movie_review_details', pk=pk)
        else:
            form = MovieForm(instance=instance)
    else:
        form = None
    context = {
        'form': form
    }
    return render(request, 'movie_reviews/edit.html', context)


# def movie_delete_view(request, id):
#     # dictionary for initial data with
#     # field names as keys
#     context ={}
 
#     # fetch the object related to passed id
#     obj = get_object_or_404(MovieReview, id = id)
 
 
#     if request.method =="POST":
#         # delete object
#         obj.delete()
#         # after deleting redirect to
#         # home page
#         return HttpResponseRedirect("movies_list")
 
#     return render(request, "movie_reviews/delete.html", context)


class MovieReviewDeleteView(LoginRequiredMixin, DeleteView):
    model = MovieReview
    context_object_name = "movie"
    template_name = "movie_reviews/delete.html"
    success_url = reverse_lazy("movies_list")


@require_http_methods(["POST"])
def new_watchlist_item(request):
    movie_id = request.POST.get("movie_id")
    movie = MovieReview.objects.get(id=movie_id)
    user = request.user
    try:
        WatchlistItem.objects.create(
            owner = user,
            movie = movie
        )
    except IntegrityError:
        pass
    return redirect("movie_review_details", pk=movie.id)


class WatchlistListView(ListView):
    model = WatchlistItem
    template_name = "watchlist/list.html"
    paginate_by = 40

    def get_queryset(self):
        return WatchlistItem.objects.filter(owner=self.request.user)


@require_http_methods(["POST"])
def delete_watchlist_item(request):
    watchlist_item_id = request.POST.get("watchlist_item_id")
    WatchlistItem.objects.filter(id=watchlist_item_id).delete()
    return redirect(
        "watchlist"
    )


@require_http_methods(["POST"])
def mark_movie_as_watched(request):
    watchlist_item_id = request.POST.get("watchlist_item_watched")
    WatchlistItem.objects.filter(id=watchlist_item_id).update(watch_status=True)
    return redirect(
        "watchlist"
    )
    pass
