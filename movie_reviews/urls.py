from django.urls import path
from movie_reviews.views import(
    new_watchlist_item,
    MoviesListView,
    # MovieReviewCreateView, 
    create_movie_review, 
    movie_review_detail, 
    update_review, 
    MovieReviewDeleteView,
    WatchlistListView,
    delete_watchlist_item,
    mark_movie_as_watched,
)

urlpatterns = [
    path("", MoviesListView.as_view(), name="movies_list"),
    # path("new_movie_review/", MovieReviewCreateView.as_view(), name="new_movie_review"),
    path("new_movie_review2/", create_movie_review, name="new_movie_review2"),
    path("full_review/<int:pk>/", movie_review_detail, name="movie_review_details"),
    path("update/<int:pk>/", update_review, name="edit_movie_review"),
    path("delete/<int:pk>", MovieReviewDeleteView.as_view(), name="delete_movie_review"),
    path("watchlist/", WatchlistListView.as_view(), name="watchlist"),
    path("watchlist/deleteitem/", delete_watchlist_item, name="delete_watchlist_item"),
    path("watchlist/new/", new_watchlist_item, name="new_watchlist_item"),
    path("watchlist/watched/", mark_movie_as_watched, name="watchlist_item_watched_urlpath"),
]
    